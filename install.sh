#!/bin/sh

# To run it, type:
# sh install.sh <filename.ovpn>

input_file=$1 # vpn config file
input_file2=$2 # vpn config file
if [ -n "${input_file2-}" ]
then
	tlbk_name=$2  # vpn name
else
	tlbk_name=$1
fi

#config
tlbk_ext=".tblk"
pass="sggk123zetax"

# create folder .tlbk
rsync -a $input_file $tlbk_name$tlbk_ext"/"

# open
open $tlbk_name$tlbk_ext

# run apple script to click the dialog
osascript <<EOD

delay 0.5

tell application "System Events"
	if "UserNotificationCenter" is in (get name of processes whose background only is true) then

		tell application "UserNotificationCenter" to activate
		keystroke return

		delay 0.5

		tell application "System Events"
			tell process "SecurityAgent"
				set value of text field 2 of scroll area 1 of group 1 of window 1 to "$pass"
				keystroke return
			end tell
		end tell

		delay 0.5

		tell application "UserNotificationCenter" to activate
		keystroke return


	end if
end tell

EOD

# delete folder
rm -rf $tlbk_name$tlbk_ext
